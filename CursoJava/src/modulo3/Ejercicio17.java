package modulo3;

import java.util.Scanner;

public class Ejercicio17 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Ingresar un numero para mostrar su tabla de multiplicar y sumar los valores pares");
		Scanner scan = new Scanner(System.in);
		int num = scan.nextInt();
		int resultadospares = 0;
		for(int i=1; i<11; i++ )
		{
			int resultado = num * i;
			System.out.println(num + "x" + i + "=" + resultado);
			if (resultado%2==0)
				resultadospares = resultadospares + resultado;
		}
		
		System.out.println("La suma de los resultados pares es = " + resultadospares);

	}

}
