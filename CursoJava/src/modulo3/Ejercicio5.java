package modulo3;

import java.util.Scanner;

public class Ejercicio5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Ingrese la posicion del equipo");
		Scanner scan = new Scanner(System.in);
		
		int posicion = scan.nextInt();
		
		if (posicion == 1) System.out.println("Medalla de oro");
		if (posicion == 2) System.out.println("Medalla de plata");
		if (posicion == 3) System.out.println("Medalla de bronce");
		if (posicion > 3) System.out.println("A seguir participando");
	}

}
