package modulo3;

import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingresar un numero para determinar a que docena pertenece");
		int numero = scan.nextInt();
		
		if (numero > 0 && numero < 13) System.out.println("pertenece a la primer docena");
		if (numero > 12 && numero < 25) System.out.println("pertenece a la segunda docena");
		if (numero > 24 && numero < 37) System.out.println("pertenece a la tercer docena");
		if (numero > 36 || numero <= 0) System.out.println("El numero esta fuera de rango");
	}

}
