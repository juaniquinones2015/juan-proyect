package modulo1;

public class Ejercicio3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("tecla de escape\t significado");
		System.out.println("\\n\t\t significa nueva linea");
		System.out.println("\\t\t\t significa un tab de espacio");
		System.out.println("\\”\t\t es para poner “ (comillas dobles) dentro del texto por ejemplo “Belencita” ");
		System.out.println("\\\\ \t\t se utiliza para escribir la \\ dentro del texto por ejemplo \\algo\\");
		System.out.println("\\’\t\t se utiliza para las ‘(comilla simple) para escribir por ejemplo ‘Princesita’");
	}

}
